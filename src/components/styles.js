import {
    StyleSheet,
    Platform
} from 'react-native';

export const Colors = {
    background: '#8E8E8E',
    button_text: Platform.OS === 'ios' ? '#1E90FF' : '#000000',
}

const styles = StyleSheet.create({
    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
    },
    button: {
        borderColor: '#8E8E8E',
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 5,
    },
    font_header1: {
        fontSize: 28,
    },
    font_header2: {
        color: '#8E8E8E',
        fontSize: 24,
    },
    font_row_header1: {
        fontSize: 20,
    },
    font_row_header2: {
        color: '#8E8E8E',
        fontSize: 16,
    },
    font_body: {

    },
    font_button: {
        color: '#8E8E8E',
        fontSize: 14,
    }
});

export default styles;