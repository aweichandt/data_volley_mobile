import React, {Component, PropTypes} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

class TabBarButton extends Component{
  displayName: 'TabBarButton'
  propTypes: {
    text: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
    isSelected: PropTypes.bool.isRequired
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.action}
        style={[styles.button, this.props.isSelected && styles.selected]}
        >
        <Text>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
};

const styles = StyleSheet.create({
  button: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  selected: {    
    backgroundColor: '#FFFFFF'
  }
});

export default TabBarButton;
