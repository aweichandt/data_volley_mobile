import React, {PropTypes} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    TextInput,
    StyleSheet,
    Platform
} from 'react-native';

import GlobalStyles from '../styles';
import NetworkImage from './network_image';
import {imageSrcForPath} from '../../utils/image_helper';

const ImgTitleDescView = React.createClass({
    propTypes: {
        editable: PropTypes.bool.isRequired,
        autoFocus: PropTypes.bool,
        imagePath: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        onImageUpdated: PropTypes.func,
        onTitleUpdated: PropTypes.func,
        onDescriptionUpdated: PropTypes.func
    },
    getInitialState() {
        return {
            edition_mode: false,
            title: this.props.title,
            description: this.props.description
        };
    },
    componentWillReceiveProps({title, description, imagePath}) {
        this.setState({...this.state, title, description});
    },
    editImage() {
        //TODO
    },
    createEditableTextView(textProperty, updateFunc) {
        //TODO autofocus & orden
        var text = this.state[textProperty];
        if(this.state.edition_mode) {
            return (
                <View>
                    <TextInput style={[styles[textProperty], styles.textEdit]}
                      ref={textProperty}
                      placeholder={textProperty}
                      value={text}
                      onChangeText={(input) => {
                        let state = {...this.state};
                        state[textProperty] = input;
                        this.setState(state);
                      }}
                      onBlur={()=>{
                        if(updateFunc)
                            updateFunc(this.state[textProperty]);
                      }} />
                    <View style={GlobalStyles.separator}/>
                </View>
            );
        }
        else if(text && text.length) {
            return (
                <Text style={styles[textProperty]}>
                    {text}
                </Text>
            );
        }
        return null;
    },
    render() {
        var imgView, editButton=null;
        if(this.state.edition_mode) {
            imgView = (
                <TouchableOpacity onPress={this.editImage}>
                    <NetworkImage 
                      style={styles.image} 
                      source={imageSrcForPath(this.props.imagePath)}>
                        <Text style={styles.image_edit_text}>edit</Text>
                    </NetworkImage>
                </TouchableOpacity>
            );
        } else {
            imgView = (<NetworkImage 
                         style={styles.image} 
                         source={imageSrcForPath(this.props.imagePath)} />);
        }
        if(this.props.editable) {
            editButton = (
                <TouchableOpacity style={[GlobalStyles.button, styles.edit_button]}
                  onPress={()=>{this.setState({...this.state, edition_mode:!this.state.edition_mode})}}>
                    <Text style={[GlobalStyles.font_button, styles.edit_button_text]}>
                    {this.state.edition_mode?'done':'edit'}
                    </Text>
                </TouchableOpacity>
            );
        }
        return (
            <View style={styles.container}>
                {imgView}
                <View style={styles.textContainer}>
                    {this.createEditableTextView('title', this.props.onTitleUpdated)}
                    {this.createEditableTextView('description', this.props.onDescriptionUpdated)}
                </View>
                {editButton}
            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        height: 90,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F5FAFF',
    },
    image: {
        height: 80,
        width: 80,
        borderRadius: 40,
        margin: 10,
        borderColor: '#8E8E8E',
        borderWidth: StyleSheet.hairlineWidth,
    },
    image_edit_text: {
        fontSize: 16,
        textAlign: 'center',
        marginTop: 32,
        marginBottom: 32,
        color: Platform.OS === 'ios' ? '#1E90FF' : '#000000',
        backgroundColor: 'transparent'
    },
    textContainer: {
        flexGrow: 1,
        flexDirection: 'column',
        backgroundColor: '#F5FAFF',
    },
    textEdit: {
        color: '#CECECE',
        backgroundColor: '#F5FAFF',
        height: 30
    },
    title: {
        fontSize: 28,
        textAlign: 'left',
        marginLeft: 10,
    },
    description: {
        fontSize: 24,
        textAlign: 'left',
        marginLeft: 10,
        color: '#8E8E8E',
    },
    edit_button: {
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    edit_button_text: {
        margin: 5,
    },
});

export default ImgTitleDescView;