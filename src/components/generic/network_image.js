import React, {PropTypes} from 'react';
import {
    View,
    ActivityIndicator,
    Image,
    StyleSheet
} from 'react-native';

const NetworkImage = React.createClass({
    propTypes: {
        source: PropTypes.object,
        onLoad: PropTypes.func,
        onError: PropTypes.func
    },
    getInitialState() {
        return {
            error: false,
            loading: false
        };
    },
    render() {
        var loader = !this.state.loading ? this.props.children:
            <View style={{
                flexGrow: 1,
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'center'
            }}>
                <ActivityIndicator style={{marginLeft:5}} />
            </View>;
        return this.state.error ?
            (<View style={this.props.style}/>) :
            (<Image
              source={this.props.source}
              style={[this.props.style]}
              onLoadStart={(e) => this.setState({error: false, loading: true})}
              onError={(e) => {
                this.setState({error: e.nativeEvent.error, loading: false});
                if(this.props.onError)this.props.onError(e);
              }}
              onLoad={() => {
                this.setState({loading: false, error: false});
                if(this.props.onLoad)this.props.onLoad();
              }} >
                {loader}
            </Image>);
    }
});

export default NetworkImage;