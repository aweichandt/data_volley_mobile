import React, {PropTypes} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    Image
} from 'react-native';

import NetworkImage from './network_image';
import {imageSrcForPath} from '../../utils/image_helper';

const ImgTitleDescRowView = React.createClass({
    propTypes: {
        touchable: PropTypes.bool.isRequired,
        imagePath: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        onPress: PropTypes.func,
        onLongPress: PropTypes.func
    },
    render() {
        var titleView = this.props.title && this.props.title.length?
            (
                <Text style={styles.title} >
                    {this.props.title}
                </Text>
            ):null;
        var descView = this.props.description && this.props.description.length?
            (
                <Text style={styles.description} >
                    {this.props.description}
                </Text>
            ):null;
        var imgView = (<NetworkImage 
                         style={styles.image} 
                         source={imageSrcForPath(this.props.imagePath)} />);
        var textView = (
            <View style={styles.textContainer} >
                {titleView}
                {descView}
            </View>
        );
        if(this.props.touchable) {
            return(
                <TouchableOpacity style={styles.container}
                    onPress={this.props.onPress}
                    onLongPress={this.props.onLongPress} >
                    {imgView}
                    {textView}
                </TouchableOpacity>
            );
        }
        return(
            <View style={styles.container}>
                {imgView}
                {textView}
            </View>
        );
            
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F5FAFF',
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 20,
        margin: 5,
        borderColor: '#8E8E8E',
        borderWidth: StyleSheet.hairlineWidth,
    },
    textContainer: {
        flexGrow: 1,
        flexDirection: 'column',
        backgroundColor: '#F5FAFF',
    },
    title: {
        fontSize: 20,
        textAlign: 'left',
        marginLeft: 10,
    },
    description: {
        fontSize: 16,
        textAlign: 'left',
        marginLeft: 10,
        color: '#8E8E8E',
    }
});

export default ImgTitleDescRowView;