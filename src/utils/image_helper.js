
const DUMMY_IMG_URL = "http://fakeimg.pl/200x200?text=";

export function dummyImagePathForName(name) {
    let text = name.split(" ")
                   .slice(0,2)
                   .map((v)=>v[0])
                   .join("%20");
    return DUMMY_IMG_URL + text;
};

export function imageSrcForPath(imagePath) {
    if(!imagePath) return null;

    if(imagePath.startsWith("http")) {
        return {uri:imagePath};
    }
    return require(imagePath);
};