import React, {PropTypes} from 'react';
import { View } from 'react-native';

import NavigationViewContainer from './navigation/navigation_container';
import DeveloperMenu from '../components/developerMenu/developer_menu';

const appView = React.createClass ({
    propTypes:{
        dispatch: PropTypes.func.isRequired
    },
    render() {
        return(
            <View style={{flexGrow:1}}>
                <NavigationViewContainer/>
                {__DEV__ && <DeveloperMenu />}
            </View>
        );
    }
});

export default appView;