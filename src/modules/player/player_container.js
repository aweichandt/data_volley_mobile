import {connect} from 'react-redux';
import {fetchPlayer} from './player_state';
import PlayerView from './player_view';

export default connect(
    (state) => {
        return {
            playerState: state.playerView
        };
    },
    (dispatch) => {
        return {
            fetchPlayerInfo: (id) => {
                dispatch(fetchPlayer(id));
            } 
        };
    }
)(PlayerView);