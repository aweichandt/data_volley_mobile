import React, {PropTypes} from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';
import GlobalStyles from '../../components/styles';
import PlayerHeader from '../../components/generic/img_title_desc_view';

const PlayerView = React.createClass({
    propTypes:{
        //from container
        playerState: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            number: PropTypes.number.isRequired,
            position: PropTypes.string.isRequired,
            imageSrc: PropTypes.string.isRequired,
        }),
        fetchPlayerInfo: PropTypes.func.isRequired,
        //from AppRouter
        id: PropTypes.number.isRequired,
    },
    componentDidMount() {
        if(this.props.id) {
            this.props.fetchPlayerInfo(this.props.id);
        }
    },

    render() {
        return(
            <View style={styles.container}>
                <PlayerHeader editable={false} 
                  title={this.props.playerState.name} 
                  description={this.props.playerState.position}
                  imagePath={this.props.playerState.imageSrc} />
                <View style={GlobalStyles.separator}/>
                <View style={styles.bodyContainer}/>
            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        backgroundColor: '#F5FCFF',
    },
    bodyContainer: {
        flex: 9,
    }
});

export default PlayerView;