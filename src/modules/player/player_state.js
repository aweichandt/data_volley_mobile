import Immutable, {Map, fromJS} from 'immutable';

import Player from '../../model/player';
import {jsonValue} from '../../model/persistence';

const initialState = {
    id: 0,
    name: '',
    number: 0,
    position: '',
    imageSrc: ''
};

const GET_PLAYER = 'player_get';

export function fetchPlayer(id) {
    return {
        type: GET_PLAYER,
        payload: id
    };
}

export default function PlayerReducer(state = initialState, action) {
    let immutableState = fromJS(state);
    let retState = immutableState;
    switch(action.type) {
        case GET_PLAYER:
            let player = Player.findById(action.payload);
            retState = immutableState.set('id', player.id)
                                     .set('name', player.name)
                                     .set('number', player.number)
                                     .set('position', player.position)
                                     .set('imageSrc', player.imageUrl);
            break;
    };
    return retState.toJS();
}