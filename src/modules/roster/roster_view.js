import React, {PropTypes} from 'react';
import {
  StyleSheet,
  ListView,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import GlobalStyles from '../../components/styles';
import TeamHeader from '../../components/generic/img_title_desc_view';
import PlayerRow from '../../components/generic/img_title_desc_row_view';

const RosterView = React.createClass ({
	propTypes: {
    //from Container
    rosterState: PropTypes.shape({
      id: PropTypes.number.isRequired,
      players: PropTypes.array.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired,
      playerAdded: PropTypes.bool.isRequired,
    }),
    fetchInfo: PropTypes.func.isRequired,
    updateTeamProperty: PropTypes.func.isRequired,
		addPlayer: PropTypes.func.isRequired,
		removePlayer: PropTypes.func.isRequired,
    pushPlayerView: PropTypes.func.isRequired,
    onPop: PropTypes.func.isRequired,
    //from AppRouter
    id: PropTypes.number.isRequired,
    editable: PropTypes.bool.isRequired,
	},
	getInitialState() {
		var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
	    return {
	      dataSource: ds.cloneWithRows(this.props.rosterState.players)
	    };
	},
  componentDidMount() {
    if(this.props.id != undefined) {
      this.props.fetchInfo(this.props.id);
    }
  },
  componentWillUnmount() {
    this.props.onPop();
  },
	componentWillReceiveProps({rosterState}) {
		this.setState({
			dataSource: this.state.dataSource.cloneWithRows(rosterState.players)
		});
    if(rosterState.playerAdded) {
      var player = rosterState.players[rosterState.players.length -1];
      this.props.pushPlayerView('Create Player', player.id);
    }
	},
  renderRow(rowData) {
      return (
      <PlayerRow touchable={true} 
        title={rowData.name}
        description={rowData.position}
        imagePath={rowData.imageUrl}
        onPress={()=>this.props.pushPlayerView('Player', rowData.id)}
        onLongPress={()=>this.props.removePlayer(rowData)} />);
  },

  renderFooter() {
      return (
          <TouchableOpacity style={[styles.add_button, GlobalStyles.button]}
              opacity={1}
              onPress={this.props.addPlayer}>
              <Text style={GlobalStyles.font_button}>Add Player</Text>
          </TouchableOpacity>
      );
  },

	render() {
		return(
			<View style={styles.container}>
        <TeamHeader editable={this.props.editable} 
          title={this.props.rosterState.name} 
          description={this.props.rosterState.description}
          imagePath={this.props.rosterState.imageUrl}
          onTitleUpdated={(newTitle) => {
            this.props.updateTeamProperty('name', newTitle);
          }}/>
        <View style={GlobalStyles.separator}/>
        <ListView style={styles.listContainer}
          dataSource={this.state.dataSource}
          renderSeparator={(sectionId, rowId) => <View key={rowId} style={GlobalStyles.separator}/>}
          renderRow={this.renderRow}
          renderFooter={this.renderFooter} />
      </View>
		);
	}
});

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#F5FCFF',
  },
  listContainer: {
    flexGrow: 9,
  },
  add_button: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FAFF',
      paddingHorizontal: 20,
      paddingVertical: 10,
  }
});

export default RosterView;