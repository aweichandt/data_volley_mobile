import {connect} from 'react-redux';
import RosterView from './roster_view';
import {AppRoutes} from '../app_router';
import {pushRoute} from '../navigation/navigation_state';
import {
	fetchInfo, 
	updateTeamProperty, 
	addPlayer, 
	removePlayer
} from './roster_state';
import {refreshTeams} from '../teams/teams_state';

export default connect(
	(state) => {
		return { 
			rosterState: state.rosterView 
		};
	},
	(dispatch) => {
		return {
			fetchInfo: (id) => {
				dispatch(fetchInfo(id));
			},
			updateTeamProperty: (id, propName, propValue) => {
				dispatch(updateTeamProperty(id, propName, propValue));
			},
			addPlayer: (player) => {
				dispatch(addPlayer(player));
			},
			removePlayer: (player) => {
				dispatch(removePlayer(player));
			},
			pushPlayerView: (title, playerId) => {
				dispatch(pushRoute({
					key: AppRoutes.player,
					title: title,
					payload: playerId
				}));
			},
			onPop: () => {
				dispatch(refreshTeams());
			}
		};
	}
)(RosterView);