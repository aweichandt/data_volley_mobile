import Immutable, {Map, fromJS} from 'immutable';

import {dummyImagePathForName} from '../../utils/image_helper';
import Team from '../../model/team';
import Player from '../../model/player';
import {jsonValue, dbSync} from '../../model/persistence';

const initialState = {
	id: 0,
	players: [],
	name: '',
	description: '',
	imageUrl: '',
	playerAdded: false,
};

const GET_TEAM_INFO = 'roster_get_info';
const ADD_PLAYER = 'roster_add_player';
const REMOVE_PLAYER = 'roster_remove_player';
const UPDATE_TEAM = 'roster_update_team';

export function fetchInfo(team_id) {
	return {
		type: GET_TEAM_INFO,
		payload: team_id
	};
};

export function addPlayer() {
	let player = {
	    name: 'New Player',
	    number: 1,
	    position: 'setter',
	    imageUrl: dummyImagePathForName('New Player')
    };
	return {
		type: ADD_PLAYER,
		payload: player
	};
};

export function removePlayer(player) {
	return {
		type: REMOVE_PLAYER,
		payload: player
	};
};

export function updateTeamProperty(propName, propValue) {
	return {
		type: UPDATE_TEAM,
		payload: {
			key: propName,
			value: propValue
		}
	};
}

export default function RosterReducer(state = initialState, action) {

	function updateTeamInfo(id) {
		let team = jsonValue(Team.findById(id));
		if(team) {
			retState = immutableState.set('id', team.id)
									 .set('name', team.name)
									 .set('description', 'none')
									 .set('imageUrl', team.imageUrl)
									 .set('players', team.players);
		}
	};

	var retState = immutableState = fromJS(state)
									.set('playerAdded', false);
	let teamId = immutableState.get('id');
	switch(action.type) {
		case GET_TEAM_INFO:
			updateTeamInfo(action.payload);
			break;
		case UPDATE_TEAM:
		//update team
			let updateObj = {};
			updateObj.id = teamId;
			updateObj[action.payload.key] = action.payload.value;
			Team.dbSync(updateObj);
		//update state
			updateTeamInfo(teamId);
			break;
		case ADD_PLAYER:
		//create player
			let newPlayer = jsonValue(Player.create(action.payload));
		//update state
			retState = immutableState.set('playerAdded', true)
									 .updateIn(['players'], list => list.push(newPlayer));
		//link player with team
			let playersObj = {};
			playersObj.id = teamId;
			playersObj['players'] = retState.get('players').toJS();
			Team.dbSync(playersObj);
			break;
		case REMOVE_PLAYER:
            let indexToRemove = state.players.findIndex((obj, index) => obj.id == action.payload.id);
			if(indexToRemove >= 0) {
				//remove from state
				let playerState = immutableState.get('players').toJS();
				playerState.splice(indexToRemove, 1);
				retState = immutableState.set('players', fromJS(playerState));
				//remove from team
				let playersObj = {};
				playersObj.id = teamId;
				playersObj['players'] = retState.get('players').toJS();
				Team.dbSync(playersObj);
				//erase player
                let realObj = Player.findById(action.payload.id);
                realObj.erase();
            }
			break;
	}
	return retState.toJS();
};