import React, {PropTypes} from 'react';
import {
    View,
    Image,
    StyleSheet,
} from 'react-native';

import Grid from './game_court_grid';
import Menu from './game_value_menu';
import Bench, {BUTTON_WIDTH} from './game_bench_view';
var courtImage = require('../../../assets/court.png');

const GameView = React.createClass({
    propTypes: {
        gameState: PropTypes.shape({
            localTeam: PropTypes.object.isRequired,
            visitorTeam: PropTypes.object.isRequired,
            localPlayers: PropTypes.array.isRequired,
            localLibero: PropTypes.number.isRequired,
            visitorPlayers: PropTypes.array.isRequired,
            visitorLibero: PropTypes.number.isRequired,
            showGrid: PropTypes.bool.isRequired,
            showValueMenu: PropTypes.bool.isRequired,
            valueMenuPosition: PropTypes.number.isRequired,
            action: PropTypes.string.isRequired,
        }),
        fetchMatch: PropTypes.func.isRequired,
        id: PropTypes.number.isRequired,
    },
    getInitialState() {
        return { menu:true };
    },
    componentDidMount() {
        if(this.props.id) {
            this.props.fetchMatch(this.props.id);
        }
    },
    componentWillReceiveProps({gameState}) {

    },

    onGridPress(position, side) {
        console.log('grid pressed on: ' + position + ', on ' + side + ' side');
        this.setState({ menu: true});
    },
    onMenuPress(value) {
        console.log('value pressed: ' + value);
        this.setState({ menu: false});
    },
    onBenchPlayerSelected(player, team) {
        console.log('selected player: ' + player.name + ' of team: ' + team.name);
    },
    render() {
        let grid = (<Grid onPress={this.onGridPress}
                      enableTop={true}
                      enableBottom={true}
                      enableTopServe={true}
                      enableBottomServe={true} />);
        let menu = (<Menu onPress={this.onMenuPress}
                      style={{position: 'absolute', top: 100, left: 100}} />);
        return (
            <View style={styles.container}>
                <Image style={styles.court}
                  source={courtImage}
                  resizeMode={Image.resizeMode.stretch}>
                    {this.state.menu?null:grid}
                    {this.state.menu?menu:null}
                </Image>
                <View style={{flexDirection: 'column'}}>
                    <Bench 
                      localTeam={this.props.gameState.localTeam} 
                      visitorTeam={this.props.gameState.visitorTeam} 
                      opened={false}
                      onPlayerSelected={(player, team)=>this.onBenchPlayerSelected(player, team)} />
                </View>
            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    court: {
        flexGrow: 1,
    }
});

export default GameView;