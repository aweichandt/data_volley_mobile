import React, {PropTypes} from 'react';
import {
    Animated,
    View,
    TouchableOpacity,
    Text,
    StyleSheet,
} from 'react-native';

const ValueMenu = React.createClass({
    propTypes: {
        onPress: PropTypes.func.isRequired,
        style: PropTypes.object,
    },

    getInitialState() {
        return {
            disabled: true,
            opacity: new Animated.Value(0)
        }
    },

    componentDidMount() {
        Animated.timing(this.state.opacity, {toValue:1, duration: 200}).start(() => {
            this.setState({...this.state, disabled: false});
        });
    },

    beforeUnmount(callback) {
        if(!this.state.disabled) {
            this.setState({...this.state, disabled: true})
            Animated.timing(this.state.opacity, {toValue:0, duration: 200}).start(callback);
        }
    },

    render() {
        let callback = (value) => this.beforeUnmount(() => this.props.onPress(value));
        return (
            <Animated.View 
              style={[this.props.style,
                      styles.circle,
                      {opacity: this.state.opacity}]}>
                <View style={styles.column}>
                    <View style={styles.row}>
                        <TouchableOpacity
                          style={styles.button}
                          opacity={1}
                          onPress={()=>callback('+')} >
                            <Text style={styles.text}>{'+'}</Text>
                        </TouchableOpacity>
                        <View style={{flex:1}}/>
                        <TouchableOpacity
                          style={styles.button}
                          opacity={1}
                          onPress={()=>callback('#')} >
                            <Text style={styles.text}>{'#'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.column}>
                    <View style={styles.row}>
                        <View style={{flex:1}}/>
                    </View>
                </View>
                <View style={styles.column}>
                    <View style={styles.row}>
                        <TouchableOpacity
                          style={styles.button}
                          opacity={1}
                          onPress={()=>callback('=')} >
                            <Text style={styles.text}>{'='}</Text>
                        </TouchableOpacity>
                        <View style={{flex:1}}/>
                        <TouchableOpacity
                          style={styles.button}
                          opacity={1}
                          onPress={()=>callback('-')} >
                            <Text style={styles.text}>{'-'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Animated.View>
        );
    }
});

const styles = StyleSheet.create({
    circle: {
        width: 90,
        height: 90,
        borderRadius: 45,
        borderColor: '#000',
        borderWidth: StyleSheet.hairlineWidth,
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
    },
    column: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
    },
    row: {
        flexGrow: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
    },
    button: {
        flexGrow: 1,
        borderRadius: 15,
        backgroundColor: '#8E8E8E',
        borderColor: '#000',
        borderWidth: StyleSheet.hairlineWidth,
    },
    text: {
        alignSelf: 'center',
        fontSize: 20,
    }
});

export default ValueMenu