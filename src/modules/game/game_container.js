import {connect} from 'react-redux';
import {
    fetchMatch
} from './game_state';
import GameView from './game_view';

export default connect(
    (state) => {
        return{
            gameState: state.gameView
        }
    },
    (dispatch) => {
        return {
            fetchMatch: (matchId) => {
                dispatch(fetchMatch(matchId));
            },
            dispatch
        }
    }
)(GameView);