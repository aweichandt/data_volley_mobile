import Immutable, {Map, fromJS} from 'immutable';

import Match from '../../model/match';
import MatchAction from '../../model/match_action';
import Team from '../../model/team';
import {jsonValue} from '../../model/persistence';

const initialState = {
    id: 0,
    actions: [],
    localTeam: {
        id: 0,
        players: [],
        name: '',
        description: '',
        imageUrl: ''
    },
    visitorTeam: {
        id: 0,
        players: [],
        name: '',
        description: '',
        imageUrl: ''
    },
    localPlayers: [0, 0, 0, 0, 0, 0],
    localLibero: 0,
    visitorPlayers: [0, 0, 0, 0, 0, 0],
    visitorLibero: 0,
    showGrid: false,
    showValueMenu: false,
    valueMenuPosition: 0,
    action: 'none'
}

const GET_MATCH = 'game_get_match';

export function fetchMatch(id) {
    return  {
        type: GET_MATCH,
        payload: id
    };
};

export default function GameReducer(state = initialState, action) {
    let immutableState = fromJS(state);
    let retState = immutableState;
    switch(action.type) {
        case GET_MATCH:
            let match =jsonValue(Match.findById(action.payload));
            let localTeam = jsonValue(Team.findById(match.localTeamId));
            let visitorTeam = jsonValue(Team.findById(match.visitorTeamId));
            retState = immutableState.set('localTeam', fromJS(localTeam))
                                     .set('visitorTeam', fromJS(visitorTeam))
                                     .set('id', match.id)
                                     .set('date', match.date)
                                     .set('actions',fromJS(match.actions));
    };
    return retState.toJS();
};