import React, {PropTypes} from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

const CourtGrid = React.createClass({
    propTypes: {
        enableTop: PropTypes.bool.isRequired,
        enableBottom: PropTypes.bool.isRequired,
        enableTopServe: PropTypes.bool.isRequired,
        enableBottomServe: PropTypes.bool.isRequired,
        onPress: PropTypes.func.isRequired,
    },

    generateSideView(positions, label, isEnable) {
        let buttons = positions.map((val) => {
            return isEnable?
            (<TouchableOpacity style={styles.button}
              opacity={1}
              onPress={() => this.props.onPress(val, label)} />):
            (<View style={{flex:1}}/>)
        });
        return (
            <View style={styles.column}>
                <View style={styles.row}>
                    {buttons[0]}
                    {buttons[1]}
                    {buttons[2]}
                    {buttons[3]}
                    {buttons[4]}
                </View>
                <View style={styles.row}>
                    {buttons[5]}
                    {buttons[6]}
                    {buttons[7]}
                    {buttons[8]}
                    {buttons[9]}
                </View>
                <View style={styles.row}>
                    {buttons[10]}
                    {buttons[11]}
                    {buttons[12]}
                    {buttons[13]}
                    {buttons[14]}
                </View>
                <View style={styles.row}>
                    {buttons[15]}
                    {buttons[16]}
                    {buttons[17]}
                    {buttons[18]}
                    {buttons[19]}
                </View>
            </View>
        );
    },

    generateServeView(enableTop, enableBottom) {
        let topButtons = [1, 6, 5].map((val) => {
            return (<TouchableOpacity style={styles.button}
                      opacity={1}
                      onPress={() => this.props.onPress(val, 'top')} />)
        });
        let bottomButtons = [5, 6, 1].map((val) => {
            return (<TouchableOpacity style={styles.button}
                      opacity={1}
                      onPress={() => this.props.onPress(val, 'bottom')} />)
        });
        let topRow = enableTop?
            (<View style={styles.row}>
                <View style={{flex:1}}/>
                {topButtons[0]}
                {topButtons[1]}
                {topButtons[2]}
                <View style={{flex:1}}/>
            </View>):
            (<View style={{flex:1}}/>);
        let bottomRow = enableBottom?
            (<View style={styles.row}>
                <View style={{flex:1}}/>
                {bottomButtons[0]}
                {bottomButtons[1]}
                {bottomButtons[2]}
                <View style={{flex:1}}/>
            </View>):
            (<View style={{flex:1}}/>);
        return (
            <View style={styles.column}>
                {topRow}
                <View style={styles.row}>
                    <View style={{flex:1}}/>
                </View>
                <View style={styles.row}>
                    <View style={{flex:1}}/>
                </View>
                <View style={styles.row}>
                    <View style={{flex:1}}/>
                </View>
                <View style={styles.row}>
                    <View style={{flex:1}}/>
                </View>
                <View style={styles.row}>
                    <View style={{flex:1}}/>
                </View>
                <View style={styles.row}>
                    <View style={{flex:1}}/>
                </View>
                {bottomRow}
            </View>
        );
    },

    render() {
        if(this.props.enableTop || this.props.enableBottom) {
            let upperView =  this.generateSideView([0, 0, 0, 0, 0,
                                                0, 1, 6, 5, 0,
                                                0, 7, 8, 9, 0,
                                                0, 2, 3, 4, 0,],
                                                'top',
                                                this.props.enableTop);
            let lowerView =  this.generateSideView([0, 4, 3, 2, 0,
                                                0, 9, 8, 7, 0,
                                                0, 5, 6, 1, 0,
                                                0, 0, 0, 0, 0,],
                                                'bottom',
                                                this.props.enableBottom);
            return (
                <View style={styles.column}>
                    {upperView}
                    {lowerView}
                </View>
            );
        }
        else if(this.props.enableTopServe || enableBottomServe) {
            return this.generateServeView(this.props.enableTopServe, 
                                          this.props.enableBottomServe);
        }
        return (<View style={{flex:1}}/>);
    }
});

const styles = StyleSheet.create({
    column: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
    },
    row: {
        flexGrow: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
    },
    button: {
        flexGrow: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        borderColor: '#8E8E8E',
        borderWidth: StyleSheet.hairlineWidth,
    }
});

export default CourtGrid;