import React, {PropTypes} from 'react';
import {
    Animated,
    View,
    Text,
    TouchableOpacity,
    ListView,
    StyleSheet,
} from 'react-native';

import GlobalStyles from '../../components/styles';
import PlayerRow from '../../components/generic/img_title_desc_row_view';

export const BUTTON_WIDTH = 50;
export const LIST_WIDTH = 350;

const GameBenchView = React.createClass({
    propTypes: {
        localTeam: PropTypes.object.isRequired,
        visitorTeam: PropTypes.object.isRequired,
        onPlayerSelected: PropTypes.func.isRequired,
    },

    getInitialState() {
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows([]),
            opened: false,
            displayingLocal: true,
            totalWidth: new Animated.Value(BUTTON_WIDTH)
        };
    },

    componentWillReceiveProps({localTeam, visitorTeam}) {
        let oldTeam = this.state.displayingLocal?this.props.localTeam:this.props.visitorTeam;
        let newTeam = this.state.displayingLocal?localTeam:visitorTeam;
        if(!newTeam) 
            return;
        if(!oldTeam || (oldTeam.id != newTeam.id)) 
            this.switchDisplayedTeam(true, newTeam);
    },

    switchDisplayedTeam(isLocalTeam, updatedTeam) {
        let team = isLocalTeam?this.props.localTeam:this.props.visitorTeam;
        if(updatedTeam) 
            team =updatedTeam;
        if(team) {
            this.setState({
                ...this.state,
                dataSource: this.state.dataSource.cloneWithRows(team.players),
                displayingLocal:isLocalTeam,
            });
        }
    },

    openView(isLocalTeam) {
        if(!this.state.opened) {
            Animated.timing(this.state.totalWidth, {toValue:(LIST_WIDTH + BUTTON_WIDTH), duration: 200}).start(() => {
                this.setState({...this.state, opened: true});
            });
        }
    },

    closeView(callback) {
        if(this.state.opened) {
            this.setState({...this.state, opened: false});
            Animated.timing(this.state.totalWidth, {toValue:BUTTON_WIDTH, duration: 200}).start(callback);
        }
    },

    renderRow(rowData) {
        return (<PlayerRow touchable={true} 
                  title={rowData.name}
                  description={rowData.position}
                  imagePath={rowData.imageUrl}
                  onLongPress={()=>{
                    this.props.onPlayerSelected(rowData, this.props.team)
                }} />);
    },

    renderButton(isLocalTeam) {
        let color = this.state.opened && (this.state.displayingLocal == isLocalTeam)?
                    '#f0f0f0':
                    '#ffffff';
        let name = isLocalTeam?
                   this.props.localTeam.name:
                   this.props.visitorTeam.name;
        return(
            <TouchableOpacity 
              style={[GlobalStyles.button, styles.button, {backgroundColor:color}]}
              onPress={()=>{
                if(this.state.opened) {
                    if(isLocalTeam == this.state.displayingLocal) {
                        this.closeView();
                    }
                }
                else {
                    this.openView(isLocalTeam);
                }
                if(isLocalTeam != this.state.displayingLocal) {
                    this.switchDisplayedTeam(isLocalTeam);
                }
            }}>
                <Text style={styles.buttonText}>
                    {name}
                </Text>
            </TouchableOpacity>
        );
    },

    render() {
        return(
            <Animated.View 
              style={[styles.container, { width: this.state.totalWidth }]}>
                <View style={{flexDirection: 'column'}}>
                    {this.renderButton(true)}
                    {this.renderButton(false)}
                </View>
                <ListView style={styles.list}
                  dataSource={this.state.dataSource}
                  renderSeparator={(sectionId, rowId) => <View key={rowId} style={GlobalStyles.separator}/>}
                  renderRow={this.renderRow} />
            </Animated.View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        backgroundColor: 'transparent',
    },
    button: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 16,
    },
    list: {
    }
});

export default GameBenchView;