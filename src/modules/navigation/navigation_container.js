import {connect} from 'react-redux';
import {pushRoute, popRoute, switchTab, navigationCompleted} from './navigation_state';
import NavigationView from './navigation_view';

export default connect(
  state => ({
    navigationState: state.navigationState
  }),
  dispatch => ({
    switchTab(index) {
      dispatch(switchTab(index));
    },
    pushRoute(index) {
      dispatch(pushRoute(index));
    },
    onNavigateBack(action) {
      dispatch(popRoute());
    },
    onNavigateCompleted(action) {
        dispatch(navigationCompleted());
    }
  })
)(NavigationView);
