import {fromJS} from 'immutable';
import {NavigationExperimental} from 'react-native';

import {AppRoutes} from '../app_router';

const {StateUtils: NavigationStateUtils} = NavigationExperimental;

// Actions
const PUSH_ROUTE = 'navigation_push_route';
const POP_ROUTE = 'navigation_pop_route';
const SWITCH_TAB = 'navigatin_switch_tab';

export function switchTab(index) {
  return {
    type: SWITCH_TAB,
    payload: index
  };
}

// Action creators
export function pushRoute(route) {
  return {
    type: PUSH_ROUTE, 
    payload: route
  };
}

export function popRoute() {
  return {type: POP_ROUTE};
}

// reducers for tabs and scenes are separate
const initialState = {
  tabs: {
    index: 0,
    routes: [
      {key: 'MatchesTab', title: 'MATCHES'},
      {key: 'TeamsTab', title: 'TEAMS'},
    ]
  },
  // Scenes for the `HomeTab` tab.
  TeamsTab: {
    index: 0,
    routes: [{key: AppRoutes.teams, title: 'Teams'}]
  },
  // Scenes for the `TestTab` tab.
  MatchesTab: {
    index: 0,
    routes: [{key: AppRoutes.match, title: 'Matches'}]
  }
};

export default function NavigationReducer(state = initialState, action) {
  var retState = immutableState = fromJS(state);
  switch (action.type) {
    case PUSH_ROUTE: {
      // Push a route into the scenes stack.
      const route = action.payload;
      const tabs = immutableState.get('tabs');
      const tabKey = tabs.getIn(['routes', tabs.get('index')]).get('key');
      const scenes = immutableState.get(tabKey).toJS();
      let nextScenes;
      // the try/catch block prevents throwing an error when the route's key pushed
      // was already present. This happens when the same route is pushed more than once.
      try {
        nextScenes = NavigationStateUtils.push(scenes, route);
      } catch (e) {
        nextScenes = scenes;
      }
      if (scenes !== nextScenes) {
        retState = immutableState.set(tabKey, fromJS(nextScenes));
      }
      break;
    }

    case POP_ROUTE: {
      // Pops a route from the scenes stack.
      const tabs = immutableState.get('tabs');
      const tabKey = tabs.getIn(['routes', tabs.get('index')]).get('key');
      const scenes = immutableState.get(tabKey).toJS();
      const nextScenes = NavigationStateUtils.pop(scenes);
      if (scenes !== nextScenes) {
        retState = immutableState.set(tabKey, fromJS(nextScenes));
      }
      break;
    }

    case SWITCH_TAB: {
      // Switches the tab.
      const tabs = NavigationStateUtils.jumpToIndex(immutableState.get('tabs').toJS(), action.payload);
      if (tabs !== immutableState.get('tabs')) {
        retState =  immutableState.set('tabs', fromJS(tabs));
      }
      break;
    }
  }
  return retState.toJS();
}
