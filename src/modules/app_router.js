/*eslint-disable react/prop-types*/
import React from 'react';
import {View, Text} from 'react-native'; 
import TeamsContainer from './teams/teams_container';
import RosterContainer from './roster/roster_container';
import PlayerContainer from './player/player_container';
import MatchContainer from './match/match_container';
import GameContainer from './game/game_container';

export const AppRoutes = {
    teams: 'Teams',
    roster: 'Roster',
    roster_edit: 'Roster Edit',
    player: 'Player',
    match: 'Match',
    game: 'Game',
    stub: 'Stub'
};
/**
 * AppRouter is responsible for mapping a navigator scene to a view
 */
export default function AppRouter(props) {
  const key = props.scene.route.key;
  const data = props.scene.route.payload;
  if (key == AppRoutes.teams) {
    return <TeamsContainer/>;
  }
  if (key === AppRoutes.roster) {
    return <RosterContainer editable={true} id={data}/>;
  }
  if (key === AppRoutes.roster_edit) {
    return <RosterContainer editable={true} id={data}/>;
  }
  if (key === AppRoutes.player) {
    return <PlayerContainer id={data}/>;
  }
  if (key === AppRoutes.match) {
    return <MatchContainer/>;
  }
  if (key === AppRoutes.game) {
    return <GameContainer id={data}/>;
  }
  if (key == AppRoutes.stub) {
    return <View><Text>{'stub'}</Text></View>;
  }

  throw new Error('Unknown navigation key: ' + key);
}
