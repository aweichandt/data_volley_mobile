import Immutable, {Map, fromJS} from 'immutable';

import Team from '../../model/team';
import {jsonValue} from '../../model/persistence';

import {dummyImagePathForName} from '../../utils/image_helper';

const initialState = {
    teams: [],
    teamAdded: false
};

const GET_TEAMS = 'teams_get';
const ADD_TEAM = 'teams_add';
const REMOVE_TEAM = 'teams_remove';

export function refreshTeams() {
    return {
        type: GET_TEAMS,
        payload: null
    };
}

export function addTeam() {
    let team = {
        name: 'New Team',
        imageUrl: dummyImagePathForName('New Team'),
        players: []
    };
    return {
        type: ADD_TEAM,
        payload: team
    };
};

export function removeTeam(team) {
    return {
        type: REMOVE_TEAM,
        payload: team
    };
};

export default function TeamsReducer(state = initialState, action) {
    var immutableState = fromJS(state).set('teamAdded', false);
    var retState = immutableState;
    switch(action.type) {
        case GET_TEAMS:
            retState = immutableState.set('teams', jsonValue(Team.find()));
            break;
        case ADD_TEAM:
            var newOBj = Team.create(action.payload);
            retState = immutableState.set('teamAdded', true)
                                     .set('teams',  jsonValue(Team.find()));
            break;
        case REMOVE_TEAM:
            if(state.teams.find((obj, index) => obj.id == action.payload.id)) {
                let realObj = Team.findById(action.payload.id);
                realObj.erase();
                retState = immutableState.set('teams',  jsonValue(Team.find()));
            }
            break;
    }
    return retState.toJS();
};