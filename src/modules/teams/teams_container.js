import {connect} from 'react-redux';
import TeamsView from './teams_view';
import {addTeam, removeTeam, refreshTeams} from './teams_state';
import {pushRoute} from '../navigation/navigation_state';
import {AppRoutes} from '../app_router';

export default connect(
    (state) => {
        return {
            teamsState: state.teamsView
        };
    },
    (dispatch) => {
        return {
            refreshTeams: () => {
                dispatch(refreshTeams());
            },
            addTeam: () => {
                dispatch(addTeam());
            },
            removeTeam: (team) => {
                dispatch(removeTeam(team));
            },
            pushRosterView: (title, id, editionMode) => {
                let key = editionMode?AppRoutes.roster_edit:AppRoutes.roster;
                dispatch(pushRoute({
                    key: key,
                    title: title,
                    payload: id
                }));
            }
        };
    }
)(TeamsView);