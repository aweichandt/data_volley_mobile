import React, {PropTypes} from 'react';
import {
    View,
    ListView,
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native';
import GlobalStyles from '../../components/styles';
import TeamRow from '../../components/generic/img_title_desc_row_view';

const TeamsView = React.createClass({
    propTypes: {
        teamsState: PropTypes.shape({
            teams: PropTypes.array.isRequired,
            teamAdded: PropTypes.bool.isRequired,
        }),
        refreshTeams: PropTypes.func.isRequired,
        addTeam: PropTypes.func.isRequired,
        removeTeam: PropTypes.func.isRequired,
        pushRosterView: PropTypes.func.isRequired
    },
    getInitialState() {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows(this.props.teamsState.teams)
        };
    },
    componentWillReceiveProps({teamsState}) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(teamsState.teams)
        });
        if(teamsState.teamAdded) {
            var team = teamsState.teams[teamsState.teams.length -1];
            this.props.pushRosterView('Create Team', team.id, true);
        }
    },

    componentDidMount() {
        this.props.refreshTeams();
    },

    onAddPressed() {
        this.props.addTeam();
    },

    renderRow(rowData) {
        //<TeamRow team={rowData} removeTeam={this.props.removeTeam}/>
        return (
            <TeamRow touchable={true}
                title={rowData.name}
                imagePath={rowData.imageUrl}
                onPress={()=>this.props.pushRosterView('Team', rowData.id, true)}
                onLongPress={() => this.props.removeTeam(rowData)} />
        );
    },

    renderFooter() {
        return (
            <TouchableOpacity style={[styles.add_button, GlobalStyles.button]}
                opacity={1}
                onPress={this.onAddPressed}>
                <Text style={GlobalStyles.font_button}>Add Team</Text>
            </TouchableOpacity>
        );
    },

    render() {
        return (
            <ListView style={styles.container}
                dataSource={this.state.dataSource}
                renderFooter={this.renderFooter} 
                renderSeparator={(sectionId, rowId) => <View key={rowId} style={GlobalStyles.separator}/>}
                renderRow={this.renderRow} />
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
    },
    add_button: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FAFF',
        paddingHorizontal: 20,
        paddingVertical: 10,
    }
});

export default TeamsView;