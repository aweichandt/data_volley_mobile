import React, {PropTypes} from 'react';
import {
    Modal,
    View,
    Text,
    Picker,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import GlobalStyles from '../../components/styles';

const MatchBeginModal = React.createClass({
    propTypes: {
        visible: PropTypes.bool.isRequired,
        teams: PropTypes.array.isRequired,
        onConfirm: PropTypes.func,
        onCancel: PropTypes.func,
    },
    getInitialState() {
        return {
            disablePlay: true,
            localId: 0,
            visitorId: 0,
            setIds: false,
        }
    },
    componentWillReceiveProps({teams}) {
        if(teams.length>1 && !this.state.setIds) {
            this.setState({
                disablePlay: false,
                localId: teams[0].id,
                visitorId: teams[1].id,
                setIds: true
            });
        }
    },
    onStartPressed() {
        if(this.state.localId && this.state.visitorId) {
            this.props.onConfirm(this.state.localId, this.state.visitorId);
        }
    },
    createPicker(propertyName) {
        return(
            <Picker
              style={styles.picker}
              itemStyle={styles.pickerItem}
              selectedValue={this.state[propertyName]}
              onValueChange={(id) => {
                let newState = this.state;
                newState[propertyName] = id;
                newState.disablePlay = newState.localId == newState.visitorId;
                this.setState(newState);
              }} >
                {this.props.teams.map((team, i) => {
                    return (
                        <Picker.Item 
                          value={team.id}
                          label={team.name}
                          key={i} />
                    );
                })}
            </Picker>
        );
    },

    renderWarning() {
        let hasTeams = this.props.teams.length > 1;
        let teamsWithPlayers = this.props.teams.filter((team) => team.players.length>=6);
        let teamsHavePlayers = teamsWithPlayers.length > 1;
        if(hasTeams && teamsHavePlayers) {
            return null;
        }
        let label = hasTeams?
                    'You need to have 6 or more players in 2 Teams':
                    'You need to create at least 2 Teams';
        label = label + ' in order to start a match';
                    
        return (
            <View style={styles.transparentContainer}>
                <View style={styles.warningContainer}>
                    <Text style={styles.centeredText} numberOfLines={5}>
                        {label}
                    </Text>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={[styles.button, GlobalStyles.button]}
                          opacity={1}
                          onPress={() => this.props.onCancel()}>
                            <Text>{'Cancel'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    },

    renderContent() {
        let warningMessage = this.renderWarning();
        if(warningMessage) return (warningMessage);
        return (
            <View style={styles.transparentContainer}>
                <View style={styles.innerContainer}>
                    {this.createPicker('localId')}
                    <Text style={styles.centeredText}>
                        {'VS'}
                    </Text>
                    {this.createPicker('visitorId')}
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={[styles.button, GlobalStyles.button]}
                          opacity={1}
                          onPress={() => this.props.onCancel()}>
                            <Text>{'Cancel'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.button, GlobalStyles.button]}
                          opacity={1}
                          disabled={this.state.disablePlay}
                          onPress={this.onStartPressed}>
                            <Text>{'Play'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    },

    render() {
        let content = this.renderContent();
        return (
            <Modal
              animationType={'none'}
              transparent={true}
              visible={this.props.visible}
              onRequestClose={() => {
                if(this.props.onCancel)
                    this.props.onCancel();
              }} >
                {content}
            </Modal>
        );
    }
});

const styles = StyleSheet.create({
    transparentContainer: {
        flexGrow: 1,
        padding: 40,
        flexDirection: 'column',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    warningContainer: {
        height: 200,
        backgroundColor: '#F5FCFF',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    centeredText: {
        fontSize: 20,
        margin: 30,
        textAlign: 'center',
    },
    innerContainer: {
        flexGrow: 1,
        borderRadius: 20,
        backgroundColor: '#F5FCFF',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'center'
    },
    button: {
        flexGrow: 1,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F5FAFF',
        margin: 20,
    },
    picker: {
        width: 200,
    },
    pickerItem: {
        fontSize: 20
    }
});

export default MatchBeginModal;