import {connect} from 'react-redux';
import {
    fetchTeams,
    fetchMatches,
    addMatch,
    deleteMatch
} from './match_state';
import {pushRoute} from '../navigation/navigation_state';
import {AppRoutes} from '../app_router';
import MatchView from './match_view';

export default connect(
    (state) => {
        return{
            matchState: state.matchView
        }
    },
    (dispatch) => {
        return {
            fetchTeams: () => {
                dispatch(fetchTeams());
            },
            fetchMatches: () => {
                dispatch(fetchMatches());
            },
            addMatch: (localId, visitorId) => {
                dispatch(addMatch(localId, visitorId));
            },
            deleteMatch: (match) => {
                dispatch(deleteMatch(match));
            },
            pushGameView: (matchId) => {
                dispatch(pushRoute({
                    key: AppRoutes.game,
                    title: 'Game',
                    payload: matchId
                }));
            }
        };
    }
)(MatchView);