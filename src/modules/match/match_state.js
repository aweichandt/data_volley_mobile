import Immutable, {Map, fromJS} from 'immutable';

import Match from '../../model/match';
import Team from '../../model/team';
import {jsonValue} from '../../model/persistence';

const initialState = {
    teams: [],
    matches: [],
    matchAdded: false,
};

const GET_MATCHES = 'match_get_matches';
const GET_TEAMS = 'match_get_teams';
const ADD_MATCH = 'matches_add_match';
const REMOVE_MATCH = 'matches_remove_match';

export function fetchMatches() {
    return {
        type: GET_MATCHES,
        payload: null
    };
}

export function fetchTeams() {
    return {
        type: GET_TEAMS,
        payload: null
    };
}

export function addMatch(localId, visitorId) {
    let match = {
        date: Date.now(),
        localTeamId: localId,
        visitorTeamId: visitorId,
        actions: []
    };
    return {
        type: ADD_MATCH,
        payload: match
    };
}

export function deleteMatch(match) {
    return {
        type: REMOVE_MATCH,
        payload: match
    };
}

export default function MatchReducer(state = initialState, action) {
    let immutableState = fromJS(state).set('matchAdded', false);
    let retState = immutableState;
    switch(action.type) {
        case GET_TEAMS:
            retState = immutableState.set('teams', jsonValue(Team.find()));
            break;
        case GET_MATCHES:
            retState = immutableState.set('matches', jsonValue(Match.find()));
            break;
        case ADD_MATCH:
            let newMatch = Match.create(action.payload);
            retState = immutableState.set('matchAdded', true)
                                     .set('matches',  jsonValue(Match.find()));
            break;
        case REMOVE_MATCH:
            if(state.matches.find((obj, index) => obj.id == action.payload.id)) {
                let realObj = Match.findById(action.payload.id);
                realObj.erase();
                retState = immutableState.set('matches',  jsonValue(Match.find()));
            }
            break;
    };
    return retState.toJS();
}