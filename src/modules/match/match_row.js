import React, {PropTypes} from 'react';
import {
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

//import NetworkImage from '../../components/generic/network-image';

const MatchRow = React.createClass({
    propTypes: {
        localTeam: PropTypes.object.isRequired,
        visitorTeam: PropTypes.object.isRequired,
        date: PropTypes.number.isRequired,
        onPress: PropTypes.func,
        onLongPress: PropTypes.func,
    },

    render() {
        let localName = this.props.localTeam.name;
        let visitorName = this.props.visitorTeam.name;
        let date = (new Date(this.props.date)).toLocaleDateString();
        return(
            <TouchableOpacity style={styles.container}
              opacity={1}
              onPress={this.props.onPress} 
              onLongPress={this.props.onLongPress}>
                <Text style={styles.team_text}> {localName} </Text>
                <Text style={styles.date_text}> {date} </Text>
                <Text style={styles.team_text}> {visitorName} </Text>
            </TouchableOpacity>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#F5FAFF',
    },
    team_text: {
        fontSize: 20,
    },
    date_text: {
        color: '#8E8E8E',
    }
});

export default MatchRow;