import React, {PropTypes} from 'react';
import {
    View,
    Text,
    ListView,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';
import GlobalStyles from '../../components/styles';
import MatchRow from './match_row';
import MatchBeginModal from './match_begin_modal';

const MatchView = React.createClass({
    propTypes:{
        matchState: PropTypes.shape({
            teams: PropTypes.array.isRequired,
            matches: PropTypes.array.isRequired,
            matchAdded: PropTypes.bool.isRequired,
        }),
        fetchTeams: PropTypes.func.isRequired,
        fetchMatches: PropTypes.func.isRequired,
        addMatch: PropTypes.func.isRequired,
        deleteMatch: PropTypes.func.isRequired,
        pushGameView: PropTypes.func.isRequired,
    },
    getInitialState() {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            displayModal: false,
            dataSource: ds.cloneWithRows(this.props.matchState.matches)
        };
    },
    componentWillReceiveProps({matchState}) {
        if(!this.props.matchState.matches.length &&
            matchState.matches.length) {
            this.setState({
                ...this.state,
                dataSource: this.state.dataSource.cloneWithRows(matchState.matches)
            });
        }
        if(matchState.matchAdded) {
            var match = matchState.matches[matchState.matches.length -1];
            this.props.pushGameView(match.id);
        }
    },

    componentDidMount() {
        this.props.fetchTeams();
        this.props.fetchMatches();
    },

    onAddPressed() {
        //TODO show modal
        this.setState({
            ...this.state,
            displayModal: true
        })
    },

    renderRow(rowData) {
        let local = this.props.matchState.teams.find((team) => team.id == rowData.localTeamId);
        let visitor = this.props.matchState.teams.find((team) => team.id == rowData.visitorTeamId);
        return (
            <MatchRow 
              localTeam={local}
              visitorTeam={visitor}
              date={rowData.date}
              onPress={()=>this.props.pushGameView(rowData.id)}
              onLongPress={() => this.props.deleteMatch(rowData)} />
        );
    },

    renderFooter() {
        return (
            <TouchableOpacity style={[styles.add_button, GlobalStyles.button]}
                opacity={1}
                onPress={this.onAddPressed}>
                <Text style={GlobalStyles.font_button}>Add Match</Text>
            </TouchableOpacity>
        );
    },

    render() {
        return (
            <View>
                <MatchBeginModal
                  visible={this.state.displayModal}
                  teams={this.props.matchState.teams}
                  onCancel={() => this.setState({
                    ...this.state,
                    displayModal: false
                  })}
                  onConfirm={(localId, visitorId) => {
                        this.setState({
                        ...this.state,
                        displayModal: false
                      });
                      this.props.addMatch(localId, visitorId);
                  }} />
                <ListView style={styles.container}
                    dataSource={this.state.dataSource}
                    renderFooter={this.renderFooter} 
                    renderSeparator={(sectionId, rowId) => <View key={rowId} style={GlobalStyles.separator}/>}
                    renderRow={this.renderRow} />
            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
    },
    add_button: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FAFF',
        paddingHorizontal: 20,
        paddingVertical: 10,
    }
});

export default MatchView;