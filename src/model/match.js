import {DatabaseObject} from './persistence';
import MatchAction from './match_action';
//Realm db Schema
import {MatchSchema} from './schema';

class Match extends DatabaseObject{
    static schema = MatchSchema;
    static childTypeClassMap = {
        MatchAction 
    };
};

export default Match;