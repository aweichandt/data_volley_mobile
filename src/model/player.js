//Realm db Schema
import {DatabaseObject} from './persistence';
import {PlayerSchema} from './schema';

class Player extends DatabaseObject{
    static schema = PlayerSchema;
};

export default Player;