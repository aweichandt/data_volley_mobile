//Realm db Schema
import {DatabaseObject} from './persistence';
import {MatchActionSchema} from './schema';

class MatchAction extends DatabaseObject{
    static schema = MatchActionSchema;
};

export default MatchAction;