import {DatabaseObject} from './persistence';
import Player from './player'//Realm db Schema
import {TeamSchema} from './schema';

class Team extends DatabaseObject{
    static schema = TeamSchema;
    static childTypeClassMap = {
        Player 
    };
};

export default Team;