import Realm from 'realm';
import {AsyncStorage} from 'react-native';


function checkClassProperty(type) {
    var defTypes = ['string', 'int', 'float', 'double', 
                    'bool', 'data', 'date','list'];
    return defTypes.indexOf(type) == -1;
}

function getUpdateActions(realmList, jsonUpdateList, isClassList) {
    function classCompare(value) {
        return (other) => other.id == value.id;
    };
    function valCompare(value) {
        return (other) => other == value;
    };
    let comparator = isClassList?classCompare:valCompare;

    actions = {};
    actions.toUpdate = realmList.reduce((list, curr, index) => {
        let jsonObject = jsonUpdateList.find(comparator(curr));
        if(jsonObject) {
            let value = curr;
            if(isClassList) 
                value = jsonValue(curr);
            if(JSON.stringify(curr) != JSON.stringify(jsonObject))
                list.push({index:index, value:curr});
        }
        return list;
    }, []);
    let parcialRemove = realmList.reduce((list, curr, index) => {
        if(!jsonUpdateList.find(comparator(curr)))
            list.push({index:index, value:curr});
        return list;
    }, []);
    actions.toRemove = parcialRemove.map((obj, i) => {
        return {index:obj.index-i, value:obj.value};
    });
    actions.toAdd = jsonUpdateList.reduce((list, curr, index) => {
        if(!realmList.find(comparator(curr)))
            list.push({index:index, value:curr});
        return list;
    }, []);
    return actions;
}

export class DatabaseObject extends Realm.Object {

//Abstract properties
    static schema = null; //Realm schema
    static childTypeClassMap = {}; //key: classname, value: Class (for custom types)
//End
    static globalId= -1;
    static async initIds() {
        let key = this.schema.name + ':id';
        AsyncStorage.getItem(key, (err, id) => {
            if(id == null) {
                this.updateGlobalId();
            } 
            else {
                this.globalId = parseInt(id);
            }
        });
    }
    static updateGlobalId() {
        let key = this.schema.name + ':id';
        this.globalId++;
        AsyncStorage.setItem(key, this.globalId.toString(), (err) =>{
            if(err) console.log('Error saving ' + key + '!!');
        });
        return this.globalId;
    }
    static create(obj) {
        let realm = Database.get();
        let createdObj = null;
        for(let key in this.schema.properties) {
            if(this.schema.properties[key].type == 'date') {
                obj[key] = new Date(obj[key]);
            }
        }
        realm.write(() => {
            let id = this.updateGlobalId();
            let properties = {...obj, id};
            createdObj = realm.create(this.schema.name, properties);
        });
        return createdObj;
    }
    static find(filter, sort) {
        let realm = Database.get();
        if(!realm) return {};
        let result = realm.objects(this.schema.name);
        if(filter) {
            result = result.filtered(filter);
        }
        if(sort) {
            result = result.sorted(sort);
        }
        return result;
    }
    static findById(id) {
        let realm = Database.get();
        if(!realm) return {};
        let result = realm.objectForPrimaryKey(this.schema.name, id);
        let returnObj = [];
        return result;
    }
    erase() {
        let ret = this;
        let realm = Database.get();
        if(!realm) return;
        realm.write(() => {
            realm.delete(this);
        });
        return ret;
    }

    //for immutable management
    jsonValue() {
        let props = this.constructor.schema.properties;
        let serializedObject = {};
        for(let property in props) {
            let propertyDef = props[property];
            let type = propertyDef.type;
            if(checkClassProperty(type)) {
                serializedObject[property] = this[property].jsonValue();
            } 
            else if(type == 'list') {
                let objectType = propertyDef.objectType;
                let isClassList = checkClassProperty(objectType);
                let list = this[property];
                serializedObject[property] = [];
                for(let i in list) {
                    let value = list[i];
                    if(isClassList) {
                        value = value.jsonValue();
                    }
                    serializedObject[property].push(value);
                }
            }
            else if(type == 'date') {
                serializedObject[property] = this[property].getTime();
            }
            else {
                serializedObject[property] = this[property];
            }
        }
        return serializedObject;
    }
    sync(jsonObject) {
        let realm = Database.get();
        if(!realm) return null;
        let props = this.constructor.schema.properties;
        let updatedProperties = {};
        for(let property in props) {
            if(Object.keys(jsonObject).indexOf(property) == -1) {
                continue;
            }
            let propertyDef = props[property];
            let type = propertyDef.type;
            if(checkClassProperty(type)) {
                Database.sync(jsonObject[property], type);//static sync
            } 
            else if(type == 'list') {
                let objectType = propertyDef.objectType;
                let isClassList = checkClassProperty(objectType);
                //for list we have to update now
                let realmList = this[property]
                let jsonList = jsonObject[property];
                let actions = getUpdateActions(realmList, jsonList, isClassList);
                console.log(JSON.stringify(actions));
                realm.write(() => {
                    for(let actionKey in actions.toUpdate) {
                        let updateAction = actions.toUpdate[actionKey];
                        if(isClassList) {
                            Database.sync(updateAction.value, objectType);
                        }
                        else {
                            realmList[updateAction.index] = updateAction.value;
                        }
                    }
                    for(let actionKey in actions.toRemove) {
                        let removeAction = actions.toRemove[actionKey];
                        realmList.splice(removeAction.index, 1);
                    }
                    for(let actionKey in actions.toAdd) {
                        let addAction = actions.toAdd[actionKey];
                        let addObject = addAction.value;
                        if(isClassList) {
                            let objClass = this.constructor.childTypeClassMap[objectType];
                            addObject = objClass.findById(addObject.id);
                        }
                        realmList.splice(addAction.index, 0, addObject);
                    }
                });
            }
            else if(type == 'date') {
                let millis = this[property].getTime();
                if(jsonObject[property] != millis) {
                    updatedProperties[property] = new Date(jsonObject[property]);
                }
            }
            else {
                if(jsonObject[property] != this[property]) {
                    updatedProperties[property] = jsonObject[property];
                }
            }
        }
        if(Object.keys(updatedProperties).length > 0) {
            updatedProperties.id = jsonObject.id;
            realm.write(() => {
                for(let key in updatedProperties) {
                    this[key] = updatedProperties[key];
                }
            });
        }
        return this;
    }
    static dbSync(jsonObject) {
        dbSync(jsonObject, this.schema.name);
    }
}


function isRealmSchemaName(name) {
    let realm = Database.get();
    let schemaNames = realm.schema.map((schema) => schema.name);
    return schemaNames.indexOf(name) != -1;
}

export function jsonValue(realmObject) {
    let realm = Database.get();
    if(!realm) return null;

    if(isRealmSchemaName(realmObject.constructor.name)) {
        return realmObject.jsonValue();
    }
    else if(typeof(realmObject) == 'object') {
        let isArray = realmObject.hasOwnProperty('length');
        let jsonObject = isArray?[]:{};
        for(let key in realmObject) {
            jsonObject[key] = jsonValue(realmObject[key])
        }
        return jsonObject;
    }
    return realmOBj;
};

export function dbSync(jsonObject, schemaName) {
    let realm = Database.get();
    if(!realm) return null;

    if(isRealmSchemaName(schemaName)) {
        if(jsonObject.hasOwnProperty('id')) {
            let obj = realm.objectForPrimaryKey(schemaName, jsonObject.id);
            obj.sync(jsonObject);
        }
        else if(typeof(jsonObject) == 'object') {
            for(let key in jsonObject) {
                dbSync(jsonObject[key]);
            }
        }
    }
};

class Database {
    static instance = null;
    static init(schema, version) {
        //init ids
        for(var i in schema) {
            schema[i].initIds();
        }
        //init realm
        this.instance = new Realm({schema: schema, 
                                schemaVersion: version});
    }
    static get() {
        return this.instance;
    }
}

export default Database;

