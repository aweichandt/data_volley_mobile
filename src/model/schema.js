
const schemaVersion = 1; //update when a schema changes
export function getSchemaVersion() {
  return schemaVersion;
};

export const PlayerSchema = {
  name: 'Player',
  version: 1, //update when schema changes
  primaryKey: 'id',
  properties: {
    id: {type: 'int'},
    name: {type: 'string'},
    number: {type: 'int'},
    position: {type: 'string'},
    imageUrl: {type: 'string'}
  }
};

export const TeamSchema = {
  name: 'Team',
  version: 1, //update when schema changes
  primaryKey: 'id',
  properties: {
    id: {type: 'int'},
    name: {type: 'string'},
    imageUrl: {type: 'string'},
    players: {type: 'list', objectType: PlayerSchema.name }
  }
};

export const MatchActionSchema = {
  name: 'MatchAction',
  version: 1, //update when schema changes
  primaryKey: 'id',
  properties: {
    id: {type: 'int'},
    action: {type: 'string'},
    position: {type: 'int'},
    destination: {type: 'int'},
    value: {type: 'int'}
  }
};

export const MatchSchema = {
  name: 'Match',
  version: 1, //update when schema changes
  primaryKey: 'id',
  properties: {
    id: {type: 'int'},
    date: {type: 'date'},
    localTeamId: {type: 'int'},
    visitorTeamId: {type: 'int'},
    actions: {type: 'list', objectType: MatchActionSchema.name}
  }
};