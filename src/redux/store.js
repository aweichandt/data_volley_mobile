import {createStore, applyMiddleware} from 'redux';
import reducer from './reducers';

function logger({ getState }) {
  return (next) => (action) => {
    console.log('will dispatch', action)

    // Call the next dispatch method in the middleware chain.
    let returnValue = next(action)

    console.log('state after dispatch', getState())

    // This will likely be the action itself, unless
    // a middleware further in chain changed it.
    return returnValue
  }
}

var middleware = function() {
    if(__DEV__) {
        return applyMiddleware(logger)
    }
    return undefined;
}();

// create the store
const store = createStore(
  reducer,
  middleware
);

export default store;