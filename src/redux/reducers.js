import {combineReducers} from 'redux';
import NavigationStateReducer from '../modules/navigation/navigation_state';
import teamsReducer from '../modules/teams/teams_state';
import rosterReducer from '../modules/roster/roster_state';
import playerReducer from '../modules/player/player_state';
import matchReducer from '../modules/match/match_state';
import gameReducer from '../modules/game/game_state';

//add in reducers and initialState
const reducers = {
    navigationState : NavigationStateReducer,
    teamsView : teamsReducer,
	rosterView : rosterReducer,
    playerView : playerReducer,
    matchView : matchReducer,
    gameView : gameReducer,
};

export default combineReducers(reducers);