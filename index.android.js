import React, { Component } from 'react';
import {AppRegistry, BackAndroid} from 'react-native';
import * as NavigationStateActions from './src/modules/navigation/navigation_state';

import {Provider} from 'react-redux';
import store from './src/redux/store';

import App from './src/modules/app_container';

import database from './src/model/persistence';
import Player from './src/model/player';
import Team from './src/model/team';
import Match from './src/model/match'
import MatchAction from './src/model/match_action';
import {getSchemaVersion} from './src/model/schema';

var schema = [Player, Team, Match, MatchAction];
var version = getSchemaVersion();

class voleyApp extends Component {
    componentWillMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.navigateBack);
        database.init(schema, version);
    },

    navigateBack() {
        const navigationState = store.getState().get('navigationState');
        const tabs = navigationState.get('tabs');
        const tabKey = tabs.getIn(['routes', tabs.get('index')]).get('key');
        const currentTab = navigationState.get(tabKey);


        // if we are in the beginning of our tab stack
        if (currentTab.get('index') === 0) {

        // if we are not in the first tab, switch tab to the leftmost one
            if (navigationState.get('index') !== 0) {
                store.dispatch(NavigationStateActions.switchTab(0));
                return true;
            }

            // otherwise let OS handle the back button action
            return false;
        }

        store.dispatch(NavigationStateActions.popRoute());
        return true;
    },
    render() {
        return (
            <Provider store={store}>
                <App/>
            </Provider>
        );
    }
}

AppRegistry.registerComponent('voleyApp', () => voleyApp);
