import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import {Provider} from 'react-redux';
import store from './src/redux/store';

import App from './src/modules/app_container';

import database from './src/model/persistence';
import Player from './src/model/player';
import Team from './src/model/team';
import Match from './src/model/match'
import MatchAction from './src/model/match_action';
import {getSchemaVersion} from './src/model/schema';

var schema = [Player, Team, Match, MatchAction];
var version = getSchemaVersion();

class voleyApp extends Component {
  componentWillMount() {
    database.init(schema, version);
  }

  render() {
    return (
      <Provider store={store}>
        <App/>
      </Provider>
    );
  }
}

AppRegistry.registerComponent('voleyApp', () => voleyApp);
